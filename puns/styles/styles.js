import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const ELEMENT_WIDTH = width - 40;
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    backgroundColor: '#3B5998',
    width: ELEMENT_WIDTH,
    padding: 10,
    alignItems: 'center',
    marginBottom: 10,
    minHeight: 30,
    maxHeight: 140
  },
  selected: {
    backgroundColor: '#ddd',
    width: ELEMENT_WIDTH,
    padding: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
  actionButton: {
    width: ELEMENT_WIDTH/5,
    padding: 15,
    alignItems: 'center'
  },
  buttonWithActions: {
    padding: 0,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  accept: {
    backgroundColor: 'green',
  },
  decline: {
    backgroundColor: 'red',
  },
  hostName: {
    marginLeft: 20
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: '500',
    fontSize: 16
  },
  text: {
    color: '#555',
    textAlign: 'center',
    fontSize: 24,
  },
  textBig: {
    color: '#555',
    textAlign: 'center',
    fontSize: 36,
    fontWeight: "600"
  },
  signature: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 1,
    width: '100%'
  },
});