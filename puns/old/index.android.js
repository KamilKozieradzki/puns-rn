/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import Meteor, { connectMeteor } from 'react-native-meteor';
import { StackNavigator } from 'react-navigation';

import Welcome from './pages/welcome/welcome.page';
import Draw from './pages/draw/draw.page';
import NewGame from './pages/new-game/new-game.page';

import { AppRegistry } from 'react-native';

const puns = StackNavigator({
        Welcome: {
          screen: Welcome,
        },
        Draw: {
          screen: Draw,
        },
        NewGame: {
          screen: NewGame
        }
      })

AppRegistry.registerComponent('puns', () => puns);