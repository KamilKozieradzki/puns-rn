import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import Meteor from 'react-native-meteor';
import styles from '../../styles/styles';

import SignatureCapture from 'react-native-signature-capture';

export default class Draw extends Component {
  constructor() {
    super();

  }

  render() {
    return (
      <View style={styles.container}>
        <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref="sign"
                    saveImageFileInExtStorage={false}
                    showNativeButtons={true}
                    showTitleLabel={true}
                    viewMode={"portrait"}/>
      </View>
    );
  }
}