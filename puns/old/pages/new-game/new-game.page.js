import React, { Component } from 'react';
// import { View, TouchableOpacity, Text } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import Meteor, { createContainer } from 'react-native-meteor';
import styles from '../../styles/styles';

import FriendsList from '../../components/friends-list/friends-list.component';
import ModeSelect from '../../components/mode-select/mode-select.component';

class NewGame extends Component {
  constructor() {
    super();
    this.state = {
        mode: undefined,
        users: []
    }
    this._modeSelect = this._modeSelect.bind(this);
    this._sendInvitations = this._sendInvitations.bind(this);
    this._handleFriend = this._handleFriend.bind(this);
  }

  _modeSelect(mode = undefined) {
    this.setState({
        mode: mode
    }) 
  }

  _sendInvitations() {
      const settings = {
          mode: this.state.mode,
          users: this.state.users
      }
      Meteor.call('newPendingGame', settings, (err, res) => {
          if(err || !res) {
              alert(err.message || err.reason || 'Something went wrong!');
          } else if(res && !err) {
              this.props.navigation.navigate('Welcome');
          }
      })
  }

  _handleFriend(id = undefined) {
    let users = this.state.users;
    let index = users.indexOf(id);
    if(index === -1) {
        users.push(id);
    } else {
        users.splice(index, 1);
    }
    this.setState({
        users
    });
  }

  render() {
    let user = this.props.user;
    let friends;
    if(user) {
        friends = user.services.facebook.friends.data;
    }
    return (
      <Container>
        {this.state.mode
        ?
        (
            <Content>
                <Content>
                    {friends?
                        <FriendsList
                        items={friends}
                        onPress={this._handleFriend}
                        onLongPressItem={()=>{}}/>
                    : null}
                </Content>
                <Button block onPress={() => this._modeSelect()}>
                        <Text>Chenge mode</Text>
                </Button>
                <Button block onPress={this._sendInvitations}>
                        <Text>Send invitations</Text>
                </Button>
            </Content>
        )
        :
        <ModeSelect onPress={this._modeSelect}/>
        }
      </Container>
    );
  }
}

export default createContainer(params=>{
  return {
    user: Meteor.user()
  };
}, NewGame)