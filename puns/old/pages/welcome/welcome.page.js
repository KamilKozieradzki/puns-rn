import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
// import { View } from 'react-native';
import styles from '../../styles/styles';
import { Container, Content, Button, Text } from 'native-base';


import FacebookLogin from '../../components/facebook-login/facebook-login.component';
import Menu from '../menu/menu.page';
import { NavigationActions } from 'react-navigation';

Meteor.connect('ws://192.168.1.24:3000/websocket');

class Welcome extends Component {
  constructor() {
    super();
    this.data = {};

    this.state = {
      signedIn: false
    };
    this._signOut = this._signOut.bind(this);
  }

  _signOut() {
    Meteor.logout();
    const navigateAction = NavigationActions.navigate({

        routeName: 'Welcome',

        params: {}
    })
    this.props.navigation.dispatch(navigateAction)
  }

  render() {
    let { user } = this.props;
    return(
      <Container>
        {
          Meteor.status().connected
          ?
          (
            user? (
              <Content>
                <Menu signOut={this._signOut} navigation={this.props.navigation} user={user}/>
                <Text>Status: {Meteor.status().status}</Text>
                {/*Facebook login here is temporary*/}
                <FacebookLogin navigation={this.props.navigation}/>
              </Content>
            ) : <FacebookLogin navigation={this.props.navigation}/>
          )
          :
          (
            <Menu signOut={this._signOut} navigation={this.props.navigation}/>
          )
        }
      </Container>
    );
  }
}

export default createContainer(params=>{
  return {
    user: Meteor.user()
  };
}, Welcome)
