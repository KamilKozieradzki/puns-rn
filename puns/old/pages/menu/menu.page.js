import React, { Component } from 'react';
// import { View, TouchableOpacity } from 'react-native';
import Meteor, { createContainer } from 'react-native-meteor';
import styles from '../../styles/styles';
import { Container, Content, Button, Text } from 'native-base';

import Invites from '../../components/invites/invites';

class Menu extends Component {
  constructor() {
    super();
    this._nativateToDraw = this._nativateToDraw.bind(this);
    this._nativateToNewGame = this._nativateToNewGame.bind(this);
  }

  _nativateToDraw() {
    this.props.navigation.navigate('Draw');
  }

  _nativateToNewGame() {
    this.props.navigation.navigate('NewGame');
  }

  render() {
    return (
      <Container>
      {
        Meteor.status().connected?
        (
          <Content>
              <Invites navigation={this.props.navigation}/>
              {/*To do:
              -Active games
              -Recent games*/}
              <Button block onPress={this._nativateToNewGame}>
                <Text navigation={this.props.navigation}>New game</Text>
              </Button>
          </Content>
        )
        :
        (
          <Content>
            <Text>Sorry!</Text>
            <Text>You are not connected with our server!</Text>
            <Text>Check Your Internet connection or try again later!</Text>
          </Content>
        )
      }
          <Content>
            <Button block onPress={this._nativateToDraw}>
              <Text>Training</Text>
            </Button>
          </Content>
        </Container>
      
    );
  }
}

export default createContainer(params=>{
  return {
  };
}, Menu)