import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Meteor, { createContainer } from 'react-native-meteor';
import styles from '../../styles/styles';

class Invites extends Component {
  constructor() {
    super();
    this._acceptInvite = this._acceptInvite.bind(this);
    this._declineInvite = this._declineInvite.bind(this);
  }

  _acceptInvite(inviteId) {
    return Meteor.call('acceptInvitation', inviteId, (err, res) => {
        if(err) {
            alert(err.message || err.reason)
        } else if(res) {
            this.props.navigation.navigate("Draw");
        }
    })
  }

  _declineInvite(inviteId) {
    return Meteor.call('declineInvitation', inviteId, (err, res) => {
        if(err) {
            alert(err.message || err.reason)
        } else if(res) {

        }
    })
  }

  _renderInvites() {
      return this.props.intives.map( invite => {
          const hostFb = Meteor.collection('users').findOne({_id: invite.host}).profile.name;
          const userFb = Meteor.user().services.facebook.name;
          const playersFb = Meteor.collection('users').findOne({_id: invite.users[0].id}).profile.name;
          let hasJoined;
          invite.users.map( user => {
              const userId = Meteor.userId();
              if(userId === user.id) {
                  hasJoined = user.joined;
              }
          })
          return (
            <TouchableOpacity key={invite._id} style={[styles.button, styles.buttonWithActions]}>
              <View>
                  <Text style={[styles.buttonText, styles.hostName]}>{hostFb !== userFb? hostFb : playersFb}</Text>
                  <Text style={[styles.buttonText, styles.hostName]}>and {invite.users.length - 1} others</Text>
              </View>
              <View style={styles.buttonWithActions}>
                {!hasJoined? <TouchableOpacity style={[styles.actionButton, styles.accept]} onPress={() => {this._acceptInvite(invite._id)}}>
                    <Text style={styles.buttonText}>&#x2713;</Text>
                </TouchableOpacity> : null}
                <TouchableOpacity style={[styles.actionButton, styles.decline]} onPress={() => {this._declineInvite(invite._id)}}>
                    <Text style={styles.buttonText}>X</Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          );
      })
  }

  render() {
    return (
        <View>
            {this._renderInvites()}
        </View>
    );
  }
}

export default createContainer(params=>{
  return {
      intives: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false})
  };
}, Invites)