import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from '../../styles/styles';

import FriendItem from './friend-item.component';

export default class FriendsList extends Component {
  constructor() {
    super();
    this.state = {
        selected: false
    }
    this._renderFriends = this._renderFriends.bind(this);
  }

  _handleSelect(id) {
      this.setState({
          selected: !this.state.selected
      });
  }
  
  _renderFriends() {
      return this.props.items.map( item => {
        return (
            <FriendItem key={item.id}
                        id={item.id}
                        name={item.name}
                        onPress={this.props.onPress}/>
        );
      });
  }

  render() {
    return (
        <View>
            {this._renderFriends()}
        </View>
    );
  }
}
