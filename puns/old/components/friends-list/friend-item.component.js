import React, { Component } from 'react';
// import { Text, View, TouchableOpacity } from 'react-native';
import styles from '../../styles/styles';
import { Container, Content, Button, Text } from 'native-base';


export default class FriendsItem extends Component {
  constructor() {
    super();
    this.state = {
        selected: false
    }
    this._handleSelect = this._handleSelect.bind(this);
  }

  _handleSelect(id) {
      this.setState({
          selected: !this.state.selected
      });
  }

  render() {
    return (
        this.state.selected?
        (
            <Content>
                <Button block success key={this.props.id}
                            onPress={() => {
                                    this._handleSelect(this.props.id);
                                    this.props.onPress(this.props.id);
                            }}
                            onLongPress={this.props.onLongPress}>
                            <Text>{this.props.name}</Text>
                </Button>
            </Content>
        )
        :
        (
            <Content>
                <Button block key={this.props.id}
                            onPress={() => {
                                    this._handleSelect(this.props.id);
                                    this.props.onPress(this.props.id);
                            }}
                            onLongPress={this.props.onLongPress}>
                            <Text>{this.props.name}</Text>
                </Button>
            </Content>
        )
    );
  }
}
