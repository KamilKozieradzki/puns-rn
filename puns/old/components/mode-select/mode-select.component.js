import React, { Component } from 'react';
// import { Text, View, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';

import styles from '../../styles/styles';

export default class ModeSelect extends Component {
  constructor() {
    super();
  }

  render() {
    return (
        <Content>
            <Button block
                              onPress={() => this.props.onPress(1)}
                              >
                              <Text style={styles.buttonText}>Solo</Text>
            </Button>
            <Button block
                              onPress={() => this.props.onPress(2)}
                              >
                              <Text style={styles.buttonText}>Duel</Text>
            </Button>
            <Button block
                              onPress={() => this.props.onPress(4)}
                              >
                              <Text style={styles.buttonText}>Team</Text>
            </Button>
            <Button block
                              onPress={() => this.props.onPress(3)}
                              >
                              <Text style={styles.buttonText}>Group</Text>
            </Button>
        </Content>
    );
  }
}
