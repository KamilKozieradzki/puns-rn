import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import Meteor from 'react-native-meteor';
import FBSDK, { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk';
import styles from '../../styles/styles';

const USER_TOKEN_KEY = 'reactnativemeteor_usertoken';

export default class FacebookLogin extends Component {
  constructor() {
    super();
    this._signInWithFacebook = this._signInWithFacebook.bind(this);
    this.status = Meteor.status();
  }

  _loginWithTokens = () => {
    const Data = Meteor.getData();
    AccessToken.getCurrentAccessToken()
    .then((res) => {
      if (res) {
        Meteor.call('login', { facebook: res }, (err, result) => {
          if(!err) {//save user id and token
            AsyncStorage.setItem(USER_TOKEN_KEY, result.token);
            Data._tokenIdSaved = result.token;
            Meteor._userIdSaved = result.id;
            Meteor._loginWithToken(result.token);
          } else {
            Meteor.logout();
          }
        });
      }
    });
  };

  _signInWithFacebook(error, result) {
    LoginManager.logInWithReadPermissions(['public_profile', 'user_friends']).then( (result) => {
      if (result.isCancelled) {
        alert('Login cancelled');
      } else {
        alert('Login success with permissions: '
          +result.grantedPermissions.toString());
          this._loginWithTokens();
      }
    },
      (error) => {
        alert('Login fail with error: ' + error);
      }
    );
  };

  render() {
    return (
        <LoginButton
          style={styles.button}
          publishPermissions={["publish_actions"]}
          onLoginFinished={this._signInWithFacebook}
          onLogoutFinished={()=> Meteor.logout()}/>
    );
  }
}
