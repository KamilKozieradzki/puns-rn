import React, { Component } from "react";
import { StyleSheet } from "react-native";
import Meteor, { createContainer } from 'react-native-meteor';

import { Container, Content, Text, View, Spinner } from "native-base";
import MainStackRouter from "./Routers/MainStackRouter";
import Login from './components/login';
import ActionBar from 'react-native-action-bar';
import { Drawer } from 'native-base';
import { NavigationActions } from 'react-navigation';

import NavigatorService from './services/navigator';
import SideBar from './components/sideDrawer';

import SignedIn from './SignedIn';

import theme from "./themes/base-theme";

import I18n from 'react-native-i18n';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connection: 'connecting'
    };
  }

  componentWillMount() {
    this.setConnectionState();
  }

  componentWillReceiveProps (nextProps) {
    this.setConnectionState(nextProps);
  }
  
  setConnectionState(props = this.props) {
    switch(props.status.status) {
      case 'connected':
        this.setState({
          connection: 'connected'
        })
        break;
      case 'connecting':
        this.setState({
          connection: 'connecting'
        })
        break;
      default:
        this.setState({
          connection: 'disconnected'
        })
    }
  }

  closeDrawer = () => {
      this.drawer._root.close()
    };
  openDrawer = () => {
    this.drawer._root.open()
  };
  
  render() {
    if(this.state.connection === 'disconnected') {
      return (
        <Container style={{flex: 1,
                   justifyContent: 'center',
                   alignItems: 'center',
                   flexDirection: 'row'}}>
          <Content>
            <Text style={{flex: 1, textAlign: 'center'}}>
              { I18n.t('app.disconnected', { locale: this.props.language }) }
            </Text>
            <Text style={{flex: 1, textAlign: 'center'}}>
              {I18n.t('app.tryagain', { locale: this.props.language })}
            </Text>
          </Content>
        </Container>
      );
    } else if(this.state.connection === 'connecting' || this.props.inProgress) {
      return (
        <Container>
          <Content>
            <Spinner />
          </Content>
        </Container>
      );
    }
    else {
      return (
        this.props.user?
          (
            <SignedIn invites = {this.props.invites} language = {this.props.language}/>
          )
        : 
          ( 
            <Login language = {this.props.language}/>
          )
      );
    }
  }
}

const MyConnectedApp = createContainer(() => {
  const user = Meteor.user() || null;
  let language;
  if(user) {
    if(user.profile.language) {
      language = user.profile.language;
    } else {
      language = 'english';
    }
  } else {
    language = 'english';
  }
  return {
    user,
    status: Meteor.status(),
    invites: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: true}),
    inProgress: Meteor.loggingIn(),
    language
  };
}, App);

export default MyConnectedApp;
