import React, { Component } from "react";
import { StyleSheet } from "react-native";
import Meteor, { createContainer } from 'react-native-meteor';

import { Container, Content, Text, View, Spinner } from "native-base";
import MainStackRouter from "./Routers/MainStackRouter";
import Login from './components/login';
import ActionBar from 'react-native-action-bar';
import { Drawer } from 'native-base';
import { NavigationActions } from 'react-navigation';

import NavigatorService from './services/navigator';
import SideBar from './components/sideDrawer';
import OneSignal from 'react-native-onesignal'; // Import package from node modules

import {BackHandler} from 'react-native';

import theme from "./themes/base-theme";

import I18n from 'react-native-i18n';

export default class SignedIn extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('registered', this.onRegistered);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(2);
    OneSignal.configure(); 
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('registered', this.onRegistered);
    OneSignal.removeEventListener('ids',  this.onIds);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
      if(Meteor.userId()){
        NavigatorService.navigate("Home", {})
      }
      return true;
  }

  onReceived(notification) {
    // alert("Notification received: ", notification);
  }

  onOpened(openResult) {
    // alert('Message: ', openResult.notification.payload.body);
    // alert('Data: ', openResult.notification.payload.additionalData);
    // alert('isActive: ', openResult.notification.isAppInFocus);
    // alert('openResult: ', openResult);
  }

  onRegistered(notifData) {
    alert("Device had been registered for push notifications!", notifData);
  }

  onIds(device) {
    Meteor.call('registerDevice', device, (err, res) => {
      if(err) {
      }
      if(res) {
      }
      
    });
  }

  closeDrawer = () => {
      this.drawer._root.close()
    };
  openDrawer = () => {
    this.drawer._root.open()
  };
  
  render() {
      return (
            <Container>
              <ActionBar
                language = {this.props.language}
                title={'GuessMaster!'}
                onTitlePress={() => NavigatorService.reset('Home')}
                leftIconName={'menu'}
                onLeftPress={() => this.openDrawer() || this.closeDrawer()}
                rightIcons={[
                    {
                        name: 'flag',
                        badge: this.props.invites.length || undefined,
                        onPress: () => NavigatorService.reset('Home')
                    },
                ]}
              />
              <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={<SideBar language = {this.props.language}/>}
                onClose={() => this.closeDrawer()} >
                  <MainStackRouter
                    language = {this.props.language}
                    ref={navigatorRef => {
                      NavigatorService.setContainer(navigatorRef);
                    }}
                  />
              </Drawer>
              
            </Container>
          )
  }
}
