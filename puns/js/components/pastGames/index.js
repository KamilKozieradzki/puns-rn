import React, { Component } from 'react';
// import { Text, View, TouchableOpacity } from 'react-native';
import Meteor, { createContainer } from 'react-native-meteor';
import { View } from 'react-native';
import {
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  CardItem,
  Card
} from "native-base";
const TimeAgo = require('react-native-timeago');
import I18n from 'react-native-i18n';

class PastGames extends Component {
  constructor() {
    super();
  }

  _renderGames() {
      return this.props.games.map( game => {
          const hostFb = Meteor.collection('users').findOne({_id: game.host}).profile.name;
          const userFb = Meteor.user().services.facebook.name;
          const playersFb = Meteor.collection('users').findOne({_id: game.users[0].id}).profile.name;
          let hasJoined;
          game.users.map( user => {
              const userId = Meteor.userId();
              if(userId === user.id) {
                  hasJoined = user.joined;
              }
          })
          return (
            <Card key={game._id}>
                <Button transparent onPress={()=> { this.props.navigation.navigate('Game', {id: game._id})}}>
                    <CardItem>
                        <Left>
                            {/*<Thumbnail source={{uri: 'Image URL'}} />*/}
                            <Body>
                                <Text>{hostFb !== userFb? hostFb : playersFb}</Text>
                                <Text note>{I18n.t('GLOBAL.and', { locale: this.props.language })} {game.users.length - 1} {I18n.t('GLOBAL.others', { locale: this.props.language })}</Text>
                            </Body>
                        </Left>
                        <Right>
                            <Text>{this.getPlaceText(game)}</Text>
                            <Text><TimeAgo time={game.createdAt} /></Text>
                        </Right>
                    </CardItem>
                </Button>
            </Card>
          );
      })
  }

  getPlaceText(game) {
      let text;
      if(game.places[0][0] === Meteor.userId()) {
          text = I18n.t('past.winner', { locale: this.props.language });
      } else {
          text = I18n.t('past.looser', { locale: this.props.language });
      }
      return text;
  }

  render() {
    return (
        <Card>
            <CardItem header>
                <Text>{I18n.t('past.title', { locale: this.props.language })}</Text>
            </CardItem>
            {this._renderGames()}
        </Card>
    );
  }
}

export default createContainer(params=>{
  return {
      games: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: true, cancelled: false, pending: false})
  };
}, PastGames)