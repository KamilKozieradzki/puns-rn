import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Content, Button, Text, Right, Body } from 'native-base';

// import styles from '../../styles/styles';

export default class ModeSelect extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <Container style={{height: "100%", width: "100%"}}>
        <View style={{flex: 1, flexDirection: 'column', flexWrap: 'wrap', alignItems: 'stretch', height: "100%"}}>
            <Button block     success large
                              style={{ width: '100%', height: '25%'}}
                              >
                              <Text>{this.props.solo}</Text>
                              <Right><Text style={{color: 'red'}}>{this.props.soon}</Text></Right>
            </Button>
            <Button block     info large
                              style={{ width: '100%', height: '25%'}}
                              >
                              <Text>{this.props.duel}</Text>
                              <Right><Text style={{color: 'red'}}>{this.props.soon}</Text></Right>
            </Button>
            <Button block     success large
                              style={{ width: '100%', height: '25%'}}
                              >
                              <Text>{this.props.team}</Text>
                              <Right><Text style={{color: 'red'}}>{this.props.soon}</Text></Right>
            </Button>
            <Button block     info large
                              style={{ width: '100%', height: '25%'}}
                              onPress={() => this.props.onPress(3)}
                              >
                              <Text>{this.props.group}</Text>
                              <Right><Text style={{color: 'red'}}>{this.props.beta}</Text></Right>
            </Button>
        </View>
      </Container>
    );
  }
}
