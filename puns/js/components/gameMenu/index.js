import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import {
  Container,
  Text,
  Button,
  Badge,
  Content,
  View,
  DeckSwiper,
  Card,
  CardItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner
} from "native-base";
import I18n from 'react-native-i18n'
import {ScrollView} from 'react-native';

export default class GameMenu extends Component {
  constructor() {
    super();
  }

  renderPoints(item) {
    return item.answerData.map( answers => {
      let userName = Meteor.collection('users').findOne({_id: answers.userId}).profile.name.split(" ");
      if(item.author === answers.userId) {
        return;
      } else {
        return (
          <Content style={{flex: 1, width: "100%"}} key={answers.userId}>
              <CardItem style={{flex: 1, width: "100%"}}>
                <Left style={{flex: 1, flexWrap: 'wrap'}}>
                  <Text>{userName[0]}</Text>
                  <Text>{userName[1]}</Text>
                </Left>
                <Right style={{flexDirection: 'row', justifyContent: 'space-around', flex: 1}}>
                  { answers.points.map((point, index) => {
                    return point?
                    <Button key={index} style={{marginRight: 10, marginLeft: 10, height: 30, width: 30, borderRadius: 15}} success></Button> 
                    : point === null? 
                      <Button key={index} style={{marginRight: 10, marginLeft: 10, height: 30, width: 30, borderRadius: 15}} disabled></Button> 
                      :
                      <Button key={index} style={{marginRight: 10, marginLeft: 10, height: 30, width: 30, borderRadius: 15}} danger></Button>
                      })
                  }
                </Right>
             </CardItem>
          </Content>
        );
      }
    })
  }

  renderAuthor(item) {
    return item.answerData.map( answers => {
      let authorName = Meteor.collection('users').findOne({_id: item.author}).profile.name;      
      if(item.author === answers.userId) {
        return (
          <Content style={{flex: 1, width: "100%"}} key={'author' + answers.userId}>
              <CardItem style={{flex: 1, width: "100%"}}>
                <Left>
                  <Text>{authorName}</Text>
                </Left>
                <Right style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                  <Text>{answers.points[0]}</Text>
                </Right>
             </CardItem>
          </Content>
        );
      } else {
        return <Content key={Math.random()}></Content>;
      }
    })
  }

  render() {
    return (
        <Container>
            <Container>
              <View>
                  {this.props.cards.length? (
                    <DeckSwiper
                      dataSource={this.props.cards}
                      renderItem={item => {
                        if(!item.answerData) {
                          return (
                            <Card style={{ elevation: 3 }} key={item._id}>
                              <CardItem style={{backgroundColor: '#339933', marginBottom: 50}}>
                                  <Body style={{alignItems:'center'}}>
                                      <Text style={{color: '#dddddd'}}>{I18n.t('game.results', { locale: this.props.language })}</Text>
                                  </Body>
                              </CardItem>
                              <CardItem cardBody style={{flexDirection: 'column'}}>
                                  {item.map(user => {
                                    return (

                                      <Body style={{flexDirection: 'row', justifyContent: 'space-around', marginBottom: 20}} key={user.place}>
                                          <Badge><Text>{user.place}</Text></Badge>
                                          <Text>{user.name}</Text>
                                          <Text>{user.points}</Text>
                                      </Body>
                                    );
                                  })}
                              </CardItem>
                          </Card>
                          );
                        }
                          return (
                            <Card style={{ elevation: 3 }} key={item._id}>
                              <CardItem style={{backgroundColor: '#339933', marginBottom: 50}}>
                                  <Body style={{alignItems:'center'}}>
                                      <Text style={{color: '#dddddd'}}>{I18n.t('game.author', { locale: this.props.language })}</Text>
                                      <Text style={{color: '#ffffff'}}>{item.authorName}</Text>
                                  </Body>

                                  <Body style={{alignItems:'center'}}>
                                      <Text style={{color: '#dddddd'}}>{I18n.t('game.round', { locale: this.props.language })}</Text>
                                      <Text style={{color: '#ffffff'}}>{item.round + 1}</Text>
                                  </Body>

                                  {item.category? <Body style={{alignItems:'center'}}>
                                    <Text style={{color: '#dddddd'}}>{I18n.t('game.category', { locale: this.props.language })}</Text>
                                    <Text style={{color: '#ffffff'}}>{item.category}</Text>
                                  </Body> : null}
                              </CardItem>
                              <CardItem cardBody style={{flexDirection: 'column'}}>

                                  {this.renderPoints(item)}
                                  {this.renderAuthor(item)}
                                  
                              </CardItem>
                          </Card>
                          )
                        }
                      }
                  />
                  ) : <Spinner />}
              </View>
            </Container>
          <View>
            {this.props.children}
          </View>
        </Container>
    );

  }
}