import React, { Component } from 'react';
import { Button, Text, Content, Spinner } from "native-base";
import { AsyncStorage } from 'react-native';

import Meteor, { createContainer } from 'react-native-meteor';
import { LoginManager, AccessToken } from 'react-native-fbsdk';

const USER_TOKEN_KEY = 'reactnativemeteor_usertoken';

class FacebookLogin extends Component {
  constructor() {
    super();
    this.handleFacebookLogin = this.handleFacebookLogin.bind(this);
    this.loginWithTokens = this.loginWithTokens.bind(this);
    this.status = Meteor.status();
  }

  componentWillMount() {
    AccessToken.getCurrentAccessToken().then(res = {
      if(res) {
        this.loginWithTokens();
      }
    })
  }

  spinner(bool) {
    return this.setState({
        spinner: bool
    })
  }

  loginWithTokens() {
    const Data = Meteor.getData();
    AccessToken.getCurrentAccessToken()
    .then((res) => {
      if (res) {
        Meteor.call('login', { facebook: res }, (err, result) => {
          if(!err) {//save user id and token
            AsyncStorage.setItem(USER_TOKEN_KEY, result.token);
            Data._tokenIdSaved = result.token;
            Meteor._userIdSaved = result.id;
            Meteor._loginWithToken(result.token);
            this.spinner(false);
          } else {
            Meteor.logout();
          }
        });
      }
    });
  };

  handleFacebookLogin () {
    this.spinner(true);
    LoginManager.logInWithReadPermissions(['public_profile', 'user_friends']).then( (result) => {
      if (result.isCancelled) {
        alert('Login cancelled');
      } else {
          this.loginWithTokens();
      }
    },
      (error) => {
        alert('Login fail with error: ' + error);
      }
    );
  }
  render () {
    return (
        <Button
          block
          onPress={this.handleFacebookLogin}
          color="#4267B2"
        >
          <Text>{this.props.text}</Text>
        </Button>
    )
  }
}

const MyConnectedApp = createContainer(() => {
  return {
    status: Meteor.status()
  };
}, FacebookLogin);

export default MyConnectedApp;