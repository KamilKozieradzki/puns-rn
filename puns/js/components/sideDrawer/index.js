import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { View } from 'react-native';
import { Container, Content, Button, Text, Right, Body, Picker, Left, Card } from 'native-base';
import I18n from 'react-native-i18n'
const FBSDK = require('react-native-fbsdk');
const {
  LoginButton,
  AccessToken
} = FBSDK;
const Item = Picker.Item;

export default class SideDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: '',
    }
  }

  componentWillMount() {
    this.setState({
      selected : this.props.language
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selected : nextProps.language
    });
  }

  onValueChange (value: string) {
    const games = Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: false}).length
    if(games) {
      alert(I18n.t('sidedrawer.finishGames', { locale: this.props.language }))
    } else {
      Meteor.call('changeLanguage', value, (err, res) => {
        if(err) {
          alert(err.reason);
        }
        if(res) {
          this.setState({
            selected : value
          });
        }
      })
    }
  }

  languageOptions() {
    return Object.keys(I18n.translations).map((lang, i) => {
      return <Item key={i} label={I18n.translations[lang].id} value={ lang } />
    });
  }

  render() {
    return (
      <Container style={{flex: 1, alignItems: 'center', backgroundColor: '#339933'}}>
        <Content style={{flex: 1, margin: 50}}>
          <Card style={{flex: 1, marginBottom: 20}}>
            <Text note style={{textAlign: 'center'}}>{I18n.t('sidedrawer.language', { locale: this.props.language })}</Text>
            <Picker
              supportedOrientations={['portrait']}
              iosHeader="Select one"
              headerBackButtonText="<"
              mode="dropdown"
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange.bind(this)}>
              {this.languageOptions()} 
            </Picker>
          </Card>
          <LoginButton
          publishPermissions={["publish_actions"]}
          onLogoutFinished={() => Meteor.logout()}/>
        </Content>
      </Container>
    );
  }
}
