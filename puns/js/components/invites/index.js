import React, { Component } from 'react';
// import { Text, View, TouchableOpacity } from 'react-native';
import Meteor, { createContainer } from 'react-native-meteor';
import {
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  CardItem,
  Card,
  Spinner
} from "native-base";
const TimeAgo = require('react-native-timeago');
import I18n from 'react-native-i18n';

class Invites extends Component {
  constructor() {
    super();
    this._acceptInvite = this._acceptInvite.bind(this);
    this._declineInvite = this._declineInvite.bind(this);
    this.state = {
        spinner: false
    }
  }

  spinner(bool) {
    this.setState({
        spinner: bool
    })
  }

  _acceptInvite(inviteId) {
    this.spinner(true);
    return Meteor.call('acceptInvitation', inviteId, (err, res) => {
        if(err) {
            alert(err.message || err.reason)
        } else if(res) {
            this.spinner(false);
        } else {
            this.spinner(false);
        }
    })
  }

  _declineInvite(inviteId) {
    return Meteor.call('declineInvitation', inviteId, (err, res) => {
        if(err) {
            alert(err.message || err.reason)
        } else if(res) {

        }
    })
  }

  _renderInvites() {
      return this.props.intives.map( invite => {
          const hostFb = Meteor.collection('users').findOne({_id: invite.host}).profile.name;
          const userFb = Meteor.user().services.facebook.name;
          const playersFb = Meteor.collection('users').findOne({_id: invite.users[0].id}).profile.name;
          let hasJoined;
          invite.users.map( user => {
              const userId = Meteor.userId();
              if(userId === user.id) {
                  hasJoined = user.joined;
              }
          })
          return (
            <Card key={invite._id}>
                <CardItem>
                    <Left>
                        {/*<Thumbnail source={{uri: 'Image URL'}} />*/}
                        <Body>
                            <Text>{hostFb !== userFb? hostFb : playersFb}</Text>
                            <Text note>{I18n.t('GLOBAL.and', { locale: this.props.language })} {invite.users.length - 1} {I18n.t('GLOBAL.others', { locale: this.props.language })}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem>
                    {!hasJoined?<Button transparent onPress={() => {this._acceptInvite(invite._id)}}>
                        <Icon active name="checkmark" />
                        <Text>{I18n.t('invites.accept', { locale: this.props.language })}</Text>
                    </Button>: null}
                    <Button transparent onPress={() => {this._declineInvite(invite._id)}}>
                        <Icon style={{color: 'red'}} active name="close" />
                        <Text style={{color: 'red'}}>{I18n.t('invites.decline', { locale: this.props.language })}</Text>
                    </Button>
                    <Right>
                        <Text><TimeAgo time={invite.createdAt} /></Text>
                    </Right>
                </CardItem>
            </Card>
          );
      })
  }

  render() {
    if(this.state.spinner) {
        return <Spinner color='blue' />;
    }
    return (
        <Card>
            <CardItem header>
                <Text>{I18n.t('invites.title', { locale: this.props.language })}</Text>
            </CardItem>
            {this._renderInvites()}
        </Card>
    );
  }
}

export default createContainer(params=>{
  return {
      intives: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: true})
  };
}, Invites)