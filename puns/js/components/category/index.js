import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
// import {Spinner} from 'react-native';
import {
  Container,
  Text,
  Button,
  Spinner
} from "native-base";
import NavigatorService from '../../services/navigator';

export default class Category extends Component {
  constructor() {
    super();
    this.state = {
      spinner: false
    }
  }

  setCategory(category, gameId) {
    this.spinner(true);
    Meteor.call('setCategory', category, gameId, (err, res) => {
        if(res) {
        }
        if(err) {
          alert(err);
        }
    })
  }

  spinner(bool) {
    return this.setState({
        spinner: bool
    })
  }

  renderCategories() {
      const categories = this.props.categories;
      const game = this.props.game;
      return categories.map( category => {
          return (
            <Button
                key={category._id}
                block
                primary
                onPress={()=> this.props.setCategory(category, this.props.game._id)}
                >
                  <Text>{category.name}</Text>
            </Button>
          );
      })
  }

  render() {
    const categories = this.props.categories;
    if(!categories || this.state.spinner) {
      return <Spinner />
    } else {
      return (
        <Container>
            {this.renderCategories()}
        </Container>
      );
    }
  }
}