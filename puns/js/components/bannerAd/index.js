import React, { Component } from "react";

import { NativeAdsManager, InterstitialAdManager, BannerView, AdSettings } from 'react-native-fbads';

AdSettings.addTestDevice(AdSettings.deviceHashedId);
// AdSettings.clearTestDevices();
const adsManager = new NativeAdsManager('1827715564213719_1845515112433764');

export default BannerAd = ({currentGames, invites, pastGames, navigation}) => {

    onBannerAdPress = () => console.log('Ad clicked!');
    onBannerAdError = (event) => console.log('Ad error :(', event.nativeEvent);
    return (
        <BannerView
          type="large"
          placementId="1827715564213719_1845515112433764"
          onPress={this.onBannerAdPress}
          onError={this.onBannerAdError}
        />
    );
}