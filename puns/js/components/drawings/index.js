import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import {
  Container,
  Text,
  Button,
  Content,
  Spinner
} from "native-base";
import {
  Image,
  Animated,
  View,
  Dimensions
} from "react-native";
import SignatureCapture from 'react-native-signature-capture';
import ProgressBar from 'react-native-progress/Bar';
import NavigatorService from '../../services/navigator';
const timer = require('react-native-timer');

export default class Drawings extends Component {
  constructor() {
    super();
    this.state = {
      overlay: true,
      spinner: true,
      timeLeft: 30,
      imageId: '',
      touched: false
    }
    this._onSaveEvent = this._onSaveEvent.bind(this);
    this._onDragEvent = this._onDragEvent.bind(this);
    this.saveSign = this.saveSign.bind(this);
  }

  spinner(bool) {
    return this.setState({
        spinner: bool
    })
  }

  saveSign() {
      this.spinner(true);
      this.refs["draw"].saveImage();
  }

  resetSign() {
      this.refs["draw"].resetImage();
      this.setState({
        touched: false
      })
  }

  _onSaveEvent(result) {
    timer.clearInterval('timeLeft');
    const {state} = this.props.navigation;
    const {game} = state.params;
    this.spinner(true);
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    // alert(result.encoded);
    var base64Icon = 'data:image/png;base64,' + result.encoded;
    Meteor.call('updateImage', base64Icon, this.state.imageId, game._id, (err, res) => {
      if(!err && res) {
        NavigatorService.navigate('Game', {id: game._id});
      }
    })
    // this.resetSign();
    // this.handleOverlay();
  }

  _onDragEvent() {
    this.setState({
      touched: true
    })
  }

  handleOverlay() {
    const {state} = this.props.navigation;
    const {puzzle, game} = state.params;
    if(this.state.overlay) {
      timer.setInterval('timeLeft', () => {
        if(this.state.timeLeft > 0) {
          this.setState({
            timeLeft: this.state.timeLeft - 1
          })
        } else {
          timer.clearInterval('timeLeft');
        }
        
      }, 1000);
      Meteor.call('saveEmptyImage', game._id, puzzle.puzzle, (err, res) => {
        if(!err && res) {
          this.setState({
            imageId: res
          })
        }
      });
    }
    this.setState({
      overlay: !this.state.overlay,
    })
  }

  componentWillReceiveProps (nextProps) {
    const {state} = nextProps.navigation;
    const {puzzle} = state.params;
    if(puzzle.puzzle) {
      this.spinner(false);
    } else {
      this.spinner(true);
    }
  }
  
  componentWillMount() {
    const {state} = this.props.navigation;
    const {puzzle} = state.params;
    if(puzzle.puzzle) {
      this.spinner(false);
    } else {
      this.spinner(true);
    }
  }

  componentWillUnmount() {
    timer.clearInterval('timeLeft');
    // alert('You should not escape!');
  }

  render() {
    const {state} = this.props.navigation;
    const {puzzle} = state.params;
    if(!puzzle) {
      return <Spinner></Spinner>
    }
    if(this.state.overlay) {
      return (
        <Container style={{flex: 1, alignItems: 'stretch'}}>
          <Button block info style={{flex: 1, alignItems: 'center'}} onPress={() => { this.handleOverlay() } }>
              <Text>{puzzle.puzzle}</Text>
          </Button>
        </Container>
      );
    }
    let {height, width} = Dimensions.get('window');
    return (
        <Container style={{flex: 1, alignItems: 'stretch'}}>
            <ProgressBar progress={this.state.timeLeft/30} width={width} />
            <Button block info>
                <Text>{puzzle.puzzle}</Text>
            </Button>
            <SignatureCapture
                    style={[{flex:1}]}
                    ref="draw"
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}/>
            {this.state.spinner? <Spinner />
            : 
            <View style={{flexDirection: 'row'}}>
              <Button block danger onPress={() => { !this.state.spinner? this.resetSign() : null } } style={{width: '50%'}}>
                  <Text>Reset</Text>
              </Button>
              <Button block success onPress={() => { (!this.state.spinner && this.state.touched)? this.saveSign() : null } } style={{width: '50%'}}>
                  <Text>Save</Text>
              </Button>
            </View>}
        </Container>
    );

  }
}