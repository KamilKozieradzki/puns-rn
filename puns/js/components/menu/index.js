import React, { Component } from "react";
import {
  Content,
  Text,
  Button,
  Container
} from "native-base";
import CurrentGames from '../currentGames';
import Invites from '../invites';
import PastGames from '../pastGames';

import BannerAd from '../bannerAd';

import I18n from 'react-native-i18n';

export default Menu = ({currentGames, invites, pastGames, navigation, language}) => {

    onBannerAdPress = () => console.log('Ad clicked!');
    onBannerAdError = (event) => console.log('Ad error :(', event.nativeEvent);
    return (
      <Container style={{flex: 1}}>
        <Button block success large
            onPress={()=>{navigation.navigate("NewGame")}}
            navigation={navigation}>
            <Text>{I18n.t('menu.newGame', { locale: language })}</Text>
        </Button>
        <Content style={{flex: 1}}>
          {currentGames.length?<CurrentGames currentGames={currentGames} navigation={navigation} language={language}/>:null}
          {invites.length?<Invites invites={invites} navigation={navigation} language={language}/>:null}
          {pastGames.length?<PastGames pastGames={pastGames} navigation={navigation} language={language}/>:null}
        </Content>
        <BannerAd />
      </Container>
    );
}