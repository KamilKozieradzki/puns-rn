import React, { Component } from "react";
import Meteor, { createContainer } from 'react-native-meteor';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right
} from "native-base";
import Menu from '../menu';
import styles from "./styles";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: 'english'
    }
  }
  componentWillReceiveProps (nextProps) {
    if(nextProps.language !== this.props.language) {
      this.setState({
        language: nextProps.language
      })
    }
  }
  
  render() {
    return (
      <Menu 
        user={this.props.user}
        invites={this.props.invites}
        currentGames={this.props.currentGames}
        pastGames={this.props.pastGames}
        navigation={this.props.navigation}
        language={this.props.language}
      />
    );
  }
}

const ConnectHome = createContainer(() => {
  const user = Meteor.user() || null;
  let language;
  if(user) {
    if(user.profile.language) {
      language = user.profile.language;
    } else {
      language = 'english';
    }
  } else {
    language = 'english';
  }
  return {
    user,
    status: Meteor.status(),
    invites: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: true}),
    currentGames: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: false}),
    pastGames: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: true, cancelled: false}),
    language
  };
}, Home);

export default ConnectHome;
