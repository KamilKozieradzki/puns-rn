import React, { Component } from "react";
import { ScrollView, View } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body
} from "native-base";
import ModeSelect from '../modeSelect';
import FriendList from '../friendList';
import Meteor, {createContainer} from 'react-native-meteor';
import { DrawerNavigator, NavigationActions } from "react-navigation";
import FacebookLogin from '../facebook-login';
import I18n from 'react-native-i18n'

class NewGame extends Component {
  constructor() {
  super();
  this.state = {
        mode: undefined,
        users: []
    }
    this._modeSelect = this._modeSelect.bind(this);
    this._sendInvitations = this._sendInvitations.bind(this);
    this._handleFriend = this._handleFriend.bind(this);
  }
  static navigationOptions = {
    header: null
  };

  _modeSelect(mode = undefined) {
    this.setState({
        mode: mode
    }) 
  }

  _sendInvitations() {
      const settings = {
          mode: this.state.mode,
          users: this.state.users
      }
      Meteor.call('newPendingGame', settings, (err, res) => {
          if(err || !res) {
              alert(err.message || err.reason || 'Something went wrong!');
          } else if(res && !err) {
              this.props.navigation.navigate('Home');
          }
      })
  }

  _handleFriend(id = undefined) {
    let users = this.state.users;
    let index = users.indexOf(id);
    if(index === -1) {
        users.push(id);
    } else {
        users.splice(index, 1);
    }
    this.setState({
        users
    });
  }
  static navigationOptions = {
    header: null
  };
  render() {
    let user = this.props.user;
    let friends;
    if(user) {
        friends = user.services.facebook.friends.data;
    }
    const { props: { name, index, list } } = this;
    if(this.state.mode) {
      return (
        <Container>
          <Content>
            {friends?
                <FriendList
                items={friends}
                onPress={this._handleFriend}
                onLongPressItem={()=>{}}
                language={this.props.language}/>
            : null}
          </Content>
          <FacebookLogin text={I18n.t('newGame.friendsRefresh', { locale: this.props.language })}/>
          <Button block info onPress={() => this._modeSelect()}>
            <Text>{I18n.t('newGame.changeMode', { locale: this.props.language })}</Text>
          </Button>
          <Button block success onPress={this._sendInvitations}>
              <Text>{I18n.t('newGame.sendInvitations', { locale: this.props.language })}</Text>
          </Button>
        </Container>
      );
    } else {
      return (
        <ModeSelect 
          onPress={this._modeSelect}
          solo={I18n.t('newGame.solo', { locale: this.props.language })}
          duel={I18n.t('newGame.duel', { locale: this.props.language })}
          team={I18n.t('newGame.team', { locale: this.props.language })}
          group={I18n.t('newGame.group', { locale: this.props.language })}
          soon={I18n.t('newGame.soon', { locale: this.props.language })}
          beta={I18n.t('newGame.group', { locale: this.props.language })}/>
      );
    }
  }
}

const Connect = createContainer(() => {
  const user = Meteor.user() || null;
  let language;
  if(user) {
    if(user.profile.language) {
      language = user.profile.language;
    } else {
      language = 'english';
    }
  } else {
    language = 'english';
  }
  return {
    user,
    status: Meteor.status(),
    invites: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: true}),
    language
  };
}, NewGame);

export default Connect;
