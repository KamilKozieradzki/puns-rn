import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Image, View, TouchableOpacity, Dimensions } from 'react-native';
import {
  Container,
  Text,
  Spinner,
  Button
} from "native-base";
import ProgressBar from 'react-native-progress/Bar';
import NavigatorService from '../../services/navigator';
import {BackHandler} from 'react-native';
const timer = require('react-native-timer');

export default class Guesses extends Component {
  constructor() {
    super();
    this.state = {
      answers: [],
      answerIndex: null,
      correct: null,
      wrong: null,
      overlay: true,
      spinner: true,
      timeLeft: 30,
      answerId: ''
    }

    this.handleGuess = this.handleGuess.bind(this);
  }

  spinner(bool) {
    return this.setState({
        spinner: bool
    })
  }

  componentWillMount() {
    this.prepareAnswers();
  }

  componentWillReceiveProps(props) {
      if(!this.state.overlay) {
        this.handleOverlay();
      }
      this.prepareAnswers(props);
  }

  resetStates() {
    this.setState({
      answers: [],
      answerIndex: null,
      correct: null,
      wrong: null,
      overlay: true,
      spinner: true
    });
  }

  prepareAnswers(props = this.props) {
    const {puzzle} = props.navigation.state.params
    Array.prototype.shuffle = function() {
        var input = this;
        
        for (var i = input.length-1; i >=0; i--) {
        
            var randomIndex = Math.floor(Math.random()*(i+1)); 
            var itemAtIndex = input[randomIndex]; 
            
            input[randomIndex] = input[i]; 
            input[i] = itemAtIndex;
        }
        return input;
    }
    let answers = puzzle.answers;
    if(answers.length === 3) {
      answers.push(puzzle.puzzle);
    }
    answers.shuffle();
    let answerIndex;
    answers.map((answer, index) => {
      if(answer === puzzle.puzzle) {
        answerIndex = index;
      }
    })
    this.setState({
      answers,
      answerIndex
    })
    this.spinner(false);
  }

  handleGuess(answer, index){
    timer.clearInterval('timeLeft');
    this.spinner(true);
    const {user, picture, game} = this.props.navigation.state.params
    Meteor.call('handleGuess', this.state.answerId, answer, picture._id, (err, res) => {
      if(res) {
       this.setState({
          correct: index
       })
       this.spinner(false);
      } else {
        this.setState({
            correct: this.state.answerIndex,
            wrong: index
        })
        this.spinner(false);
      }
      setTimeout(() => {
        return NavigatorService.navigate("Game", {
          id: game._id
        })
      }, 3000);
    });
  }

  getStyle(ans) {
    if(this.state.correct === ans) {
      return {width: '50%', height: '50%', backgroundColor: 'green'};
    } else if(this.state.wrong === ans) {
      return {width: '50%', height: '50%', backgroundColor: 'red'};
    } else {
      return {width: '50%', height: '50%'};
    }
  }

  handleOverlay() {
    const {user, picture, game, puzzle} = this.props.navigation.state.params
    if(this.state.overlay) {
      timer.setInterval('timeLeft', () => {
        if(this.state.timeLeft > 0) {
          this.setState({
            timeLeft: this.state.timeLeft - 0.1
          })
        } else {
          timer.clearInterval('timeLeft');
          this.handleGuess(undefined, 5);
        }
      }, 100);
      this.setState({
        overlay: !this.state.overlay
      })
      Meteor.call('initGuess', user._id, picture._id, (err, res) => {
        this.setState({
          answerId: res
        })
      });
    } else {
      this.resetStates();
      this.setState({
        overlay: !this.state.overlay
      })
    }
    
  }

  componentWillUnmount() {
    if(timer.intervalExists('timeLeft')){
      timer.clearInterval('timeLeft');
    }
  }

  renderButtons() {
    return this.state.answers.map((answer, index) => {
      if(index === 1 || index === 2) {
        return (
          <Button key={index} block primary onPress={() => { !this.state.spinner? this.handleGuess(answer, index) : null } } style={this.getStyle(index)} >
              <Text>{answer}</Text>
          </Button>
        );
      } else {
        return (
          <Button key={index} block info onPress={() => { !this.state.spinner? this.handleGuess(answer, index) : null } } style={this.getStyle(index)} >
              <Text>{answer}</Text>
          </Button>
        );
      }
    })
  }

  render() {
    const {picture, category} = this.props.navigation.state.params
    // alert(picture);
    if(!picture || this.state.spinner) {
      return <Spinner />
    }
    if(this.state.overlay) {
      return (
        <Container style={{flex: 1, alignItems: 'stretch'}}>
          <Button info full ><Text>{category.name}</Text></Button>
          <TouchableOpacity style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'stretch',
              flexDirection: 'row'}} onPress={() => { this.handleOverlay() }}>
              {!!!picture.image? 
                <Text
                  style={{
                    flex: 1, textAlign: 'center', marginTop: '60%'
                  }}>
                  Sorry! Author did not draw a picture. You have to shoot!
                </Text>
                :
                <Image
                  source={{
                    uri: picture.image
                  }}
                  style={{
                    flex: 1
                  }}
                />}
          </TouchableOpacity>
        </Container>
      );
    }
    let {height, width} = Dimensions.get('window');
    return (
        <Container style={{height: "100%"}}>
                <ProgressBar progress={this.state.timeLeft/30} width={width} />
              {/*{!this.state.answers? <Spinner /> : (*/}
                <View style={{height: "100%"}}>
                  <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'stretch', height: "100%"}}>
                    {this.renderButtons()}
                  </View>
                </View>
              {/*)}*/}
        </Container>
    );

  }
}