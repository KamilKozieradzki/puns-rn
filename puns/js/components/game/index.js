import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import {
  Container,
  Spinner,
  Text
} from "native-base";
import GameComponent from './gameComponent';
import NavigatorService from '../../services/navigator';
const timer = require('react-native-timer');


class Game extends Component {
  constructor() {
    super();
    this.state = {
      stage: null,
      spinner: true,
      categoriesProps: null,
      puzzle: null,
      drawnum: null,
      picture: null,
      category: null
    }
    this.spinner = this.spinner.bind(this);
    this.setCategory = this.setCategory.bind(this);
  }

  spinner(bool) {
    return this.setState({
        spinner: bool
    })
  }

  componentWillMount() {
    const {categoriesProps} = this.getGameToShow();
    this.setState({
        categoriesProps
    }, this.setUserStage(this.props.game, this.props.pictures));
  }

  componentWillReceiveProps (nextProps = this.props) {
    const { categoriesProps } = this.getGameToShow(nextProps);
    if(categoriesProps) {
      this.setState({
          categoriesProps
      }, this.setUserStage(nextProps.game, nextProps.pictures));
    } else {
      this.setUserStage(nextProps.game, nextProps.pictures);
    }
  }

  getGameToShow(nextProps = this.props) {
    const game = this.props.game;
    let categoriesProps;
    if(game) {
      categoriesProps = game.categoryProps;
      return {
          categoriesProps
      };
    } else {
      return {
        categoriesProps: ''
      }
    }
  }

  setCategory(category, gameId) {
    this.spinner(true);
    Meteor.call('setCategory', category, gameId, (err, res) => {
        this.spinner(false);
    })
  }

  setUserStage(game, pictures) {
      let roundData = {};
      if(game && game.roundData) {
        roundData = game.roundData[game.round];
      }
      this.setState({
        category: roundData.category
      })
      game.queue = game.queue || [false];
      const isUserFirstOnQueue = game.queue[0].userId === this.props.user._id;
      let userNum;
      if(!isUserFirstOnQueue) {
        game.queue.map( (user, index) => {
            if(user.userId === Meteor.userId()) {
                userNum = index;
            }
        })
      }
      if(isUserFirstOnQueue && !roundData.category) {
        this.setState({
                stage: 'Category'
        })
      } else if(isUserFirstOnQueue && roundData.category && game.queue[0].status !== 'pending') {
        const drawnum = pictures.length
        const puzzle = roundData.puzzles[drawnum];
        if(!puzzle) {
          this.setState({
            stage: ''
          })
        } else {
          this.setState({
                stage: 'Drawings',
                drawnum,
                puzzle
          })
        }
      } else if(!isUserFirstOnQueue && roundData.category && game.queue[userNum].status !== 'pending' && game.queue[0].status === 'pending') {
        const drawnum = this.props.answers.length;
        const puzzle = roundData.puzzles[drawnum];
        if(!puzzle) {
          this.setState({
            stage: ''
          })
        } else {
          const picture = pictures.filter(item => {
            if(item.puzzle === puzzle.puzzle) {
              return item;
            }
          })[0];
          this.setState({
                  stage: 'Guesses',
                  drawnum,
                  puzzle,
                  picture
          })
        }
      } else {
        this.setState({
                stage: ''
        })
      }
      this.spinner(false);
  };

  componentWillUnmount() {
    this.setState({
      stage: null,
      categoriesProps: null,
      puzzle: null,
      drawnum: null,
      picture: null,
      category: null
    })
  }
  
  render() {
    if(this.state.spinner || !this.props.game) {
        return <Spinner />
    } else {
        return (
            <Container>
              <GameComponent
                  categories = {this.state.categoriesProps}
                  saveImage = {this.saveImage}
                  stage = {this.state.stage}
                  game = {this.props.game}
                  category = {this.state.category}
                  puzzle = {this.state.puzzle}
                  picture = {this.state.picture}
                  user = {this.props.user}
                  drawnum = {this.state.drawnum}
                  setCategory = {this.setCategory}
                  language={this.props.language}
              />
            </Container>
        );
    }
  }
}

export default createContainer(params=>{
  const gameId = params.navigation.state.params.id;
  let game = Meteor.collection('games').findOne({"users.id": Meteor.userId(), cancelled: false, _id: gameId});
  const user = Meteor.user() || null;
  let language;
  if(user) {
    if(user.profile.language) {
      language = user.profile.language;
    } else {
      language = 'english';
    }
  } else {
    language = 'english';
  }
  return {
      game,
      user,
      categories: Meteor.collection('categories').find({language}),
      answers: Meteor.collection('answers').find({gameId, userId: Meteor.userId(), round: game.round}),
      pictures: Meteor.collection('pictures').find({gameId, round: game.round, image: {$ne: undefined}}),
      language
  };
}, Game)