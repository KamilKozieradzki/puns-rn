import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Alert } from 'react-native';
import {
  Container,
  Spinner,
  Text,
  Button,
  Badge,
  Content
} from "native-base";
import GameMenu from '../gameMenu';
import Category from '../category';
import NavigatorService from '../../services/navigator';

import I18n from 'react-native-i18n'

import BannerAd from '../bannerAd';

import { NativeAdsManager, InterstitialAdManager, BannerView, AdSettings } from 'react-native-fbads';

AdSettings.addTestDevice(AdSettings.deviceHashedId);

export default class GameComponent extends Component {
  constructor() {
    super();
    this.state = {
        cards: []
    }
  }
  showFullScreenAd = () => {
    InterstitialAdManager.showAd('1827715564213719_1844187202566555')
      .then(didClick => {
        console.log(didClick);
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentWillReceiveProps (nextProps) {
      Meteor.call('initCards', nextProps.game, (err, res) => {
        if(res){
            this.setState({
                cards: res
            })
        }
      })
  }

  componentDidMount() {
    this.showFullScreenAd();
    Meteor.call('initCards', this.props.game, (err, res) => {
      if(res){
        this.setState({
          cards: res
        });
      }
    })
  }

  calculateBadge() {
    const maxPictures = this.props.game.settings.pictures;
    const drawNum = this.props.drawnum;
    if(this.props.stage === 'Drawings') {
        return (maxPictures - drawNum) || maxPictures;
    } else if(this.props.stage === 'Category') {
        return maxPictures + 1;
    } else if(this.props.stage === 'Guesses') {
        return (maxPictures - drawNum) || maxPictures
    }
  }

  getWinnerName() {
      const name = Meteor.collection('users').findOne({_id: this.props.game.places[0][0]}).profile.name;
      return <Text>{name}</Text>;
  }

  render() {
    if(this.props.stage === 'Category') {
        return (
                <Category
                    categories = {this.props.categories}
                    game = {this.props.game}
                    user = {this.props.user}
                    setCategory = {this.props.setCategory}
                />
            );
    }
            
    return (
        <GameMenu
            game = {this.props.game}
            user = {this.props.user}
            cards = {this.state.cards}
            language = {this.props.language}
        >            
            {this.props.stage && this.props.stage !== 'Category'?
            (
                <Button
                full
                success
                onPress={
                    ()=> { 
                        NavigatorService.navigate(this.props.stage, {
                            categories: this.props.categories,
                            game: this.props.game,
                            puzzle: this.props.puzzle,
                            picture: this.props.picture,
                            user: this.props.user,
                            category: this.props.category,
                            language: this.props.language
                        })
                    }
                }
                >
                    <Text>{I18n.t('game.continue', { locale: this.props.language })}</Text>
                    <Badge>
                        <Text>{this.calculateBadge()}</Text>
                    </Badge>
                </Button>
            ): this.props.game.places[0]?
            (
                <Button full success>
                    <Text>{I18n.t('game.winner', { locale: this.props.language })}</Text>
                    {this.getWinnerName()}
                </Button>
            ): (
                <Button full disabled>
                    <Text>{I18n.t('game.wait', { locale: this.props.language })}</Text>
                </Button>
            )}
            <BannerAd />
        </GameMenu>
    );
  }
}