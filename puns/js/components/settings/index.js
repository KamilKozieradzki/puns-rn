import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body
} from "native-base"
import Meteor, {createContainer} from 'react-native-meteor';
const FBSDK = require('react-native-fbsdk');
const {
  LoginButton,
  AccessToken
} = FBSDK;

class Settings extends Component {
  constructor() {
  super();
  }

  render() {
    return (
      <Container style={{flex: 1, alignItems: 'center'}}>
        <Content style={{flex: 1}}>
          <LoginButton
            publishPermissions={["publish_actions"]}
            onLogoutFinished={() => Meteor.logout()}/>
        </Content>
      </Container>
    );
  }
}

const Connect = createContainer(() => {
  return {
    user: Meteor.user(), 
    status: Meteor.status(),
    invites: Meteor.collection('games').find({"users.id": Meteor.userId(), finished: false, cancelled: false, pending: true})
  };
}, Settings);

export default Connect;
