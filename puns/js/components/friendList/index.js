import React, { Component } from 'react';
// import { View, TouchableOpacity, Text } from 'react-native';
// import styles from '../../styles/styles';
import Meteor, { createContainer } from 'react-native-meteor';

import {
  Container,
  Content,
  Text
} from "native-base";

import FriendItem from './item';

export default class FriendList extends Component {
  constructor() {
    super();
    this.state = {
        selected: false
    }
    this._renderFriends = this._renderFriends.bind(this);
  }

  _handleSelect(id) {
      this.setState({
          selected: !this.state.selected
      });
  }
  
  _renderFriends() {
      return this.props.items.map( item => {
        const friend = Meteor.collection('users').findOne({"services.facebook.id": item.id});
        let friendLanguage;
        if(friend) {
            friendLanguage = friend.profile.language;
        }
        if(friendLanguage === this.props.language) {
            return (
                <FriendItem key={item.id}
                            id={item.id}
                            name={item.name}
                            onPress={this.props.onPress}/>
            );
        } else {
            return null;
        }
      });
  }

  render() {
    return (
        <Content>
            {this._renderFriends()}
        </Content>
    );
  }
}
