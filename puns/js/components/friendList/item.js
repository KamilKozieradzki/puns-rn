import React, { Component } from 'react';
// import { Text, View, TouchableOpacity } from 'react-native';
// import styles from '../../styles/styles';
import { Container, Content, Button, Text } from 'native-base';


export default class FriendItem extends Component {
  constructor() {
    super();
    this.state = {
        selected: false
    }
    this._handleSelect = this._handleSelect.bind(this);
  }

  _handleSelect(id) {
      this.setState({
          selected: !this.state.selected
      });
  }

  render() {
    return (
        this.state.selected?
        (
            <Button block info key={this.props.id}
                        onPress={() => {
                                this._handleSelect(this.props.id);
                                this.props.onPress(this.props.id);
                        }}
                        onLongPress={this.props.onLongPress}>
                        <Text>{this.props.name}</Text>
            </Button>
        )
        :
        (
            <Button block light key={this.props.id}
                        onPress={() => {
                                this._handleSelect(this.props.id);
                                this.props.onPress(this.props.id);
                        }}
                        onLongPress={this.props.onLongPress}>
                        <Text>{this.props.name}</Text>
            </Button>
        )
    );
  }
}
