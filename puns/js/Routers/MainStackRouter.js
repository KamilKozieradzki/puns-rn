import React, { Component } from "react";
import Login from "../components/login/";
import Home from "../components/home/";
import NewGame from "../components/newGame";
import Game from "../components/game";
import Settings from "../components/settings";
import Drawings from "../components/drawings";
import Guesses from "../components/guesses";
import Category from "../components/category";
import { StackNavigator } from "react-navigation";
import { Header, Left, Button, Icon, Body, Title, Right } from "native-base";
export default (StackNav = StackNavigator({
  Home: { screen: Home },
  NewGame: { screen: NewGame },
  Game: { screen: Game },
  Settings: {screen: Settings },
  Drawings: {screen: Drawings },
  Guesses: {screen: Guesses },
  Category: {screen: Category },
},{ 
    headerMode: 'none' 
}));
