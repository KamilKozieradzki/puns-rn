
import React, { Component } from 'react';
import Meteor, { connect } from 'react-native-meteor';
import { StyleProvider } from 'native-base';
import App from './App';
import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';
import '../I18n/I18n';


function setup():React.Component {
  class Root extends Component {

  constructor() {
    super();
    this.state = {
      isLoading: false,
    };
  }

  componentWillMount() {
    Meteor.connect('ws://192.168.1.24:3000/websocket');
    // Meteor.connect('ws://puns-vetagdfeou.now.sh/websocket');

  }
  
  render() {
      return (
        <StyleProvider style={getTheme(platform)}>
            <App />
        </StyleProvider>
      );
    }
  }

  return Root;
}

export default setup;


